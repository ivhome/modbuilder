import argparse
import os
import json
from zipfile import ZipFile

parser = argparse.ArgumentParser(description = "Factorio mod builder")
parser.add_argument("mod_root", help = "path to mod root")
parser.add_argument("--rebuild", action = "store_true",
    help = "overwrite build if exists")
args = parser.parse_args()
mod_root = args.mod_root
rebuild = args.rebuild

mod_files = {}
for root, dirs, files in os.walk(mod_root, topdown = True):
    dirs[:] = [d for d in dirs if not d.startswith(".")]
    if root not in mod_files:
        mod_files[root] = [file for file in files if not file.startswith(".")]

if "info.json" not in mod_files[mod_root]:
    raise Exception("'info.json' not found")

mod_info = json.load(open(os.path.join(mod_root, "info.json")))
mod_name = "{name}_{version}".format(**mod_info)
build_path = os.path.abspath(os.path.join(mod_root, os.pardir))
build_file = os.path.join(build_path, "{0}.zip".format(mod_name))

if os.path.isfile(build_file) and not rebuild:
    raise Exception("File {0} already exists".format(build_file))

print("Creating archive {0}...".format(build_file))
with ZipFile(build_file, "w") as archiver:
    for path, files in mod_files.items():
        for file in map(lambda f: os.path.join(path, f), files):
            archname = os.path.join(mod_name, os.path.relpath(file, mod_root))
            archiver.write(file, arcname = archname)
            print(file)
print("Done!")
